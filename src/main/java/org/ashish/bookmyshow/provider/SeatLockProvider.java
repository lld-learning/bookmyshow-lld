package org.ashish.bookmyshow.provider;

import org.ashish.bookmyshow.models.Booking;
import org.ashish.bookmyshow.models.Seat;

import java.util.List;

public interface SeatLockProvider {

    void lockSeats(Booking booking,List<Seat> seats);
    void unlockSeats(Booking booking,List<Seat> seats);
    List<Seat> getLockedSeats(Booking booking);
}
