package org.ashish.bookmyshow.provider;

import org.ashish.bookmyshow.models.Booking;
import org.ashish.bookmyshow.models.Seat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemorySeatLockProvider implements SeatLockProvider{
    private final Map<String,List<Seat>> lockedSeatMap;

    public InMemorySeatLockProvider() {
        this.lockedSeatMap = new ConcurrentHashMap<>();
    }

    @Override
    public void lockSeats(Booking booking, List<Seat> seats) {
        lockedSeatMap.put(booking.getId(),seats);
    }

    @Override
    public void unlockSeats(Booking booking, List<Seat> seats) {
        lockedSeatMap.remove(booking.getId());
    }

    @Override
    public List<Seat> getLockedSeats(Booking booking) {
        return lockedSeatMap.get(booking.getId());
    }
}
