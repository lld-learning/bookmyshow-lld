package org.ashish.bookmyshow.services;

import org.ashish.bookmyshow.models.Booking;
import org.ashish.bookmyshow.models.Customer;
import org.ashish.bookmyshow.models.MovieShow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class BookingService {
    Map<String, Booking> bookingMap=new HashMap<>();

    public Booking createBooking(Customer customer, MovieShow movieShow){
        String bookingId= UUID.randomUUID().toString();
        Booking booking=new Booking(bookingId,customer,movieShow);
        bookingMap.put(bookingId,booking);
        return booking;
    }
}
