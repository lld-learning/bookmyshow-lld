package org.ashish.bookmyshow.models;

import lombok.NonNull;

import java.util.List;

public class Theater {
    private final List<Screen> screens;
    private final List<MovieShow> movieShows;

    public Theater(@NonNull final List<Screen> screens,
                   @NonNull final List<MovieShow> movieShows) {
        this.screens = screens;
        this.movieShows = movieShows;
    }
}
