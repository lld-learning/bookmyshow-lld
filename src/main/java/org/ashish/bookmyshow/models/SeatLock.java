package org.ashish.bookmyshow.models;

import java.util.Date;

public class SeatLock {
    private Seat seat;
    private MovieShow movieShow;
    private Integer timeout;
    private Date lockTime;
    private String lockedBy;
}
