package org.ashish.bookmyshow.models;

import lombok.Getter;
import org.ashish.bookmyshow.models.enums.PaymentMode;
import org.ashish.bookmyshow.models.enums.PaymentStatus;

@Getter
public class Payment {
    private final String id;
    private final String movieShowId;
    private PaymentMode paymentMode;
    private PaymentStatus paymentStatus;

    public Payment(String id, String movieShowId, PaymentMode paymentMode) {
        this.id = id;
        this.movieShowId = movieShowId;
        this.paymentMode = paymentMode;
        this.paymentStatus=PaymentStatus.STARTED;
    }
}
