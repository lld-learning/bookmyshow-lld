package org.ashish.bookmyshow.models;

import lombok.Getter;
import org.ashish.bookmyshow.models.enums.BookingStatus;

import java.util.List;

@Getter
public class Booking {
    private final String id;
    private final Customer customer;
    private MovieShow movieShow;
    private List<Seat> seatList;
    private Payment payment;
    private BookingStatus bookingStatus;

    public Booking(String id, Customer customer, MovieShow movieShow) {
        this.id = id;
        this.customer = customer;
        this.movieShow = movieShow;
        this.bookingStatus=BookingStatus.STARTED;
    }
}
