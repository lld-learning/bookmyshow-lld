package org.ashish.bookmyshow.models.enums;

public enum ShowTimings {
    MORNING,
    AFTERNOON,
    EVENING;
}
