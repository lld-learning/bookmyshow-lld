package org.ashish.bookmyshow.models.enums;

public enum BookingStatus {
    STARTED,COMPLETED;
}
