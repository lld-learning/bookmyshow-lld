package org.ashish.bookmyshow.models.enums;

public enum PaymentStatus {
    STARTED,
    SUCCESS,
    FAILED;
}
