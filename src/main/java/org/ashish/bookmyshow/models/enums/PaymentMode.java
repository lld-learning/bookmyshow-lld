package org.ashish.bookmyshow.models.enums;

public enum PaymentMode {
    CASH,
    CARD,
    DIGITAL_WALLET;
}
