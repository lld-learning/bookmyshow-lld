package org.ashish.bookmyshow.models.enums;

public enum SeatStatus {
    UNLOCKED,
    LOCKED,
    BOOKED;
}
