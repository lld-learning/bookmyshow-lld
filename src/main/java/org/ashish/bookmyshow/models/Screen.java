package org.ashish.bookmyshow.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Screen {
    private final long id;
    private final String type;
    private final List<Seat> seats;


}
