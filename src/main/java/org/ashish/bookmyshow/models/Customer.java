package org.ashish.bookmyshow.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public abstract class Customer {
    private String name;
}
