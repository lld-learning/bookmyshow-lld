package org.ashish.bookmyshow.models;

import lombok.Getter;
import org.ashish.bookmyshow.exceptions.InvalidSeatStatusException;
import org.ashish.bookmyshow.models.enums.SeatStatus;

@Getter
public class Seat {
    private final int seatNumber;
    private SeatStatus seatStatus;

    public Seat(int seatNumber) {
        this.seatNumber = seatNumber;
        this.seatStatus=SeatStatus.UNLOCKED;
    }

    public void unlockSeat(){
        if(this.seatStatus==SeatStatus.BOOKED)
            throw new InvalidSeatStatusException();
        this.seatStatus=SeatStatus.UNLOCKED;
    }

    public void lockSeat(){
        if(this.seatStatus==SeatStatus.BOOKED)
            throw new InvalidSeatStatusException();
        this.seatStatus=SeatStatus.LOCKED;
    }

    public void markSeatBooked(){
        if(this.seatStatus==SeatStatus.LOCKED)
            this.seatStatus=SeatStatus.BOOKED;
        else
            throw new InvalidSeatStatusException();

    }
}
