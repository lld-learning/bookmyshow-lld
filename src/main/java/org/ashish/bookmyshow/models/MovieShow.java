package org.ashish.bookmyshow.models;

import lombok.NonNull;
import org.ashish.bookmyshow.models.enums.ShowTimings;

public class MovieShow {
    private String id;
    private final Screen screen;
    private final Movie movie;
    private final ShowTimings showTimings;

    public MovieShow(@NonNull final Screen screen,
                     @NonNull final Movie movie,
                     @NonNull final ShowTimings showTimings) {
        this.screen = screen;
        this.movie = movie;
        this.showTimings = showTimings;
    }
}
