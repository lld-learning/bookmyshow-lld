package org.ashish.bookmyshow.controllers;

import org.ashish.bookmyshow.models.MovieShow;
import org.ashish.bookmyshow.models.Screen;
import org.ashish.bookmyshow.models.Theater;

import java.util.List;

public class TheaterController {

    public Theater createTheater(List<Screen> screens, List<MovieShow> movieShows){
        return new Theater(screens,movieShows);
    }


}
