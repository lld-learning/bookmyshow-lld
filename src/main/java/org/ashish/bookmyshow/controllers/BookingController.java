package org.ashish.bookmyshow.controllers;

import lombok.NonNull;
import org.ashish.bookmyshow.models.Booking;
import org.ashish.bookmyshow.models.Customer;
import org.ashish.bookmyshow.models.MovieShow;
import org.ashish.bookmyshow.models.Seat;
import org.ashish.bookmyshow.services.BookingService;

import java.util.List;

public class BookingController {
    private final BookingService bookingService;

    public BookingController(@NonNull final BookingService bookingService) {
        this.bookingService = bookingService;
    }

    public Booking createBooking(Customer customer, MovieShow movieShow){
        return bookingService.createBooking(customer,movieShow);
    }

    public void selectSeats(Customer customer,Booking booking,MovieShow movieShow, List<Seat> seatList){

    }
}
